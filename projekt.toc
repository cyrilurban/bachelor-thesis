\babel@toc {czech}{}
\contentsline {chapter}{\numberline {1}\IeC {\'U}vod}{3}{chapter.1}
\contentsline {chapter}{\numberline {2}Technologie aplikac\IeC {\'\i } na platform\IeC {\v e} JavaScript}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}Architektura klient-server na platform\IeC {\v e} JavaScript}{4}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Architektura klient-server}{4}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Jazyk JavaScript a jeho vyu\IeC {\v z}it\IeC {\'\i }}{6}{subsection.2.1.2}
\contentsline {section}{\numberline {2.2}Mo\IeC {\v z}n\IeC {\'e} p\IeC {\v r}\IeC {\'\i }stupy v\IeC {\'y}voje mobiln\IeC {\'\i }ch aplikac\IeC {\'\i }}{7}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Nativn\IeC {\'\i } aplikace}{7}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Multiplatformn\IeC {\'\i } aplikace zalo\IeC {\v z}en\IeC {\'e} na webov\IeC {\'y}ch technologi\IeC {\'\i }ch}{8}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Multiplatformn\IeC {\'\i } aplikace p\IeC {\v r}ekl\IeC {\'a}dan\IeC {\'e} do nativn\IeC {\'\i }ho k\IeC {\'o}du}{9}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Multiplatformn\IeC {\'\i } aplikace interpretovan\IeC {\'e} za b\IeC {\v e}hu}{9}{subsection.2.2.4}
\contentsline {section}{\numberline {2.3}Multiplatformn\IeC {\'\i } aplikace interpretovan\IeC {\'e} za b\IeC {\v e}hu v~JavaScriptu}{10}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Princip funkcionality}{10}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Nativn\IeC {\'\i } a sd\IeC {\'\i }len\IeC {\'y} JavaScriptov\IeC {\'y} k\IeC {\'o}d}{10}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}P\IeC {\v r}\IeC {\'\i }stup k~nativn\IeC {\'\i }m funkc\IeC {\'\i }m}{11}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Vyu\IeC {\v z}it\IeC {\'\i } frameworku nad JavaScriptem}{12}{subsection.2.3.4}
\contentsline {subsection}{\numberline {2.3.5}Open-source}{12}{subsection.2.3.5}
\contentsline {section}{\numberline {2.4}NativeScript}{12}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}V\IeC {\'y}vojov\IeC {\'e} mo\IeC {\v z}nosti}{12}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Stylovan\IeC {\'\i } a animace}{13}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Popularita a velikost komunity}{13}{subsection.2.4.3}
\contentsline {section}{\numberline {2.5}React Native}{14}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}V\IeC {\'y}vojov\IeC {\'e} mo\IeC {\v z}nosti}{15}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}Stylovan\IeC {\'\i } a animace}{15}{subsection.2.5.2}
\contentsline {subsection}{\numberline {2.5.3}Popularita a velikost komunity}{15}{subsection.2.5.3}
\contentsline {section}{\numberline {2.6}Srovn\IeC {\'a}n\IeC {\'\i } NativeScript vs React Native}{16}{section.2.6}
\contentsline {section}{\numberline {2.7}Popis zvolen\IeC {\'y}ch technologi\IeC {\'\i }}{17}{section.2.7}
\contentsline {subsection}{\numberline {2.7.1}NativeScript}{17}{subsection.2.7.1}
\contentsline {subsection}{\numberline {2.7.2}EcmaScript 2015 (ES6)}{17}{subsection.2.7.2}
\contentsline {subsubsection}{Nahrazen\IeC {\'\i } var za let a const}{17}{section*.16}
\contentsline {subsubsection}{Snadn\IeC {\v e}j\IeC {\v s}\IeC {\'\i } definice funkc\IeC {\'\i }}{17}{section*.17}
\contentsline {subsubsection}{Promise nam\IeC {\'\i }sto callback}{17}{section*.18}
\contentsline {subsubsection}{Nov\IeC {\'a} definice t\IeC {\v r}\IeC {\'\i }d}{18}{section*.19}
\contentsline {subsubsection}{Dal\IeC {\v s}\IeC {\'\i } zaj\IeC {\'\i }mav\IeC {\'e} funkce}{18}{section*.20}
\contentsline {subsection}{\numberline {2.7.3}CSS pre-procesor SASS pro NativeScript}{18}{subsection.2.7.3}
\contentsline {subsubsection}{CSS pre-procesor}{18}{section*.21}
\contentsline {subsubsection}{Pre-procesor SASS}{19}{section*.22}
\contentsline {subsubsection}{SASS pro NativeScript}{19}{section*.23}
\contentsline {subsection}{\numberline {2.7.4}Node.js a serverov\IeC {\'e} technologie}{19}{subsection.2.7.4}
\contentsline {subsubsection}{Node.js}{19}{section*.24}
\contentsline {subsubsection}{Npm}{20}{section*.25}
\contentsline {subsubsection}{Express.js}{20}{section*.26}
\contentsline {subsection}{\numberline {2.7.5}Datab\IeC {\'a}zov\IeC {\'e} technologie}{21}{subsection.2.7.5}
\contentsline {subsubsection}{Knex.js}{21}{section*.27}
\contentsline {subsubsection}{Datab\IeC {\'a}ze Sqlite3}{21}{section*.28}
\contentsline {chapter}{\numberline {3}N\IeC {\'a}vrh aplikace}{23}{chapter.3}
\contentsline {section}{\numberline {3.1}Zad\IeC {\'a}n\IeC {\'\i } aplikace}{23}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Typ a objekt prost\IeC {\v r}edku}{23}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Akce}{24}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Vyhled\IeC {\'a}v\IeC {\'a}n\IeC {\'\i } voln\IeC {\'y}ch prost\IeC {\v r}edk\IeC {\r u}}{24}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}Rezervace}{24}{subsection.3.1.4}
\contentsline {subsection}{\numberline {3.1.5}Opakov\IeC {\'a}n\IeC {\'\i }}{24}{subsection.3.1.5}
\contentsline {subsection}{\numberline {3.1.6}Zobrazen\IeC {\'\i } rezervac\IeC {\'\i }}{24}{subsection.3.1.6}
\contentsline {section}{\numberline {3.2}Podobn\IeC {\'e} aplikace}{25}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}My Time}{25}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Hub Spot}{25}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Google kalend\IeC {\'a}\IeC {\v r}}{25}{subsection.3.2.3}
\contentsline {section}{\numberline {3.3}Serverov\IeC {\'a} \IeC {\v c}\IeC {\'a}st}{26}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Datab\IeC {\'a}zov\IeC {\'y} model}{26}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}N\IeC {\'a}vrh aplika\IeC {\v c}n\IeC {\'\i }ho rozhran\IeC {\'\i }}{27}{subsection.3.3.2}
\contentsline {section}{\numberline {3.4}Klientsk\IeC {\'a} \IeC {\v c}\IeC {\'a}st}{27}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}N\IeC {\'a}vrh pr\IeC {\'a}ce s~daty -- MVVM}{27}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}N\IeC {\'a}vrh grafick\IeC {\'e}ho u\IeC {\v z}ivatelsk\IeC {\'e}ho rozhran\IeC {\'\i }}{28}{subsection.3.4.2}
\contentsline {subsubsection}{Nov\IeC {\'a} rezervace}{28}{section*.29}
\contentsline {subsubsection}{M\IeC {\r u}j kalend\IeC {\'a}\IeC {\v r}}{28}{section*.30}
\contentsline {subsubsection}{Kalend\IeC {\'a}\IeC {\v r} objektu}{28}{section*.31}
\contentsline {subsubsection}{Potvrzen\IeC {\'\i } rezervac\IeC {\'\i }}{29}{section*.32}
\contentsline {chapter}{\numberline {4}Implementace \IeC {\v r}e\IeC {\v s}en\IeC {\'\i }}{30}{chapter.4}
\contentsline {section}{\numberline {4.1}Server}{30}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Autentizace a sezen\IeC {\'\i }}{30}{subsection.4.1.1}
\contentsline {subsubsection}{Ukl\IeC {\'a}d\IeC {\'a}n\IeC {\'\i } hesel}{30}{section*.33}
\contentsline {subsubsection}{Proces p\IeC {\v r}ihl\IeC {\'a}\IeC {\v s}en\IeC {\'\i }}{31}{section*.34}
\contentsline {subsubsection}{Sezen\IeC {\'\i }}{31}{section*.35}
\contentsline {subsection}{\numberline {4.1.2}Vyhled\IeC {\'a}v\IeC {\'a}n\IeC {\'\i } voln\IeC {\'y}ch prost\IeC {\v r}edk\IeC {\r u}}{31}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Opakov\IeC {\'a}n\IeC {\'\i } rezervace}{32}{subsection.4.1.3}
\contentsline {subsubsection}{Pl\IeC {\'a}nov\IeC {\'a}n\IeC {\'\i } podle CRON form\IeC {\'a}tu}{33}{section*.36}
\contentsline {subsubsection}{Generov\IeC {\'a}n\IeC {\'\i } term\IeC {\'\i }n\IeC {\r u} na z\IeC {\'a}klad\IeC {\v e} CRON form\IeC {\'a}tu}{33}{section*.37}
\contentsline {subsubsection}{Omezen\IeC {\'\i } CRON form\IeC {\'a}tu a jeho \IeC {\v r}e\IeC {\v s}en\IeC {\'\i }}{34}{section*.38}
\contentsline {subsection}{\numberline {4.1.4}Upozorn\IeC {\v e}n\IeC {\'\i } prost\IeC {\v r}ednictv\IeC {\'\i }m e-mailu}{34}{subsection.4.1.4}
\contentsline {subsubsection}{Odes\IeC {\'\i }l\IeC {\'a}n\IeC {\'\i } email\IeC {\r u}}{34}{section*.39}
\contentsline {subsubsection}{Pl\IeC {\'a}nov\IeC {\'a}n\IeC {\'\i } \IeC {\'u}loh}{34}{section*.40}
\contentsline {section}{\numberline {4.2}Klient}{35}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Uk\IeC {\'a}zkov\IeC {\'e} aplikace}{35}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Nativn\IeC {\'\i } komponenty}{36}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Kalend\IeC {\'a}\IeC {\v r}}{37}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Intuitivn\IeC {\'\i } vyhled\IeC {\'a}v\IeC {\'a}n\IeC {\'\i } osob}{39}{subsection.4.2.4}
\contentsline {subsection}{\numberline {4.2.5}Nedostatky technologie NativeScript}{40}{subsection.4.2.5}
\contentsline {chapter}{\numberline {5}Testov\IeC {\'a}n\IeC {\'\i }}{41}{chapter.5}
\contentsline {section}{\numberline {5.1}Serverov\IeC {\'e} testov\IeC {\'a}n\IeC {\'\i }}{41}{section.5.1}
\contentsline {section}{\numberline {5.2}Klientsk\IeC {\'e} testov\IeC {\'a}n\IeC {\'\i }}{41}{section.5.2}
\contentsline {section}{\numberline {5.3}U\IeC {\v z}ivatelsk\IeC {\'e} testov\IeC {\'a}n\IeC {\'\i }}{42}{section.5.3}
\contentsline {chapter}{\numberline {6}Z\IeC {\'a}v\IeC {\v e}r}{43}{chapter.6}
\contentsline {section}{\numberline {6.1}Roz\IeC {\v s}\IeC {\'\i }\IeC {\v r}en\IeC {\'\i } pr\IeC {\'a}ce}{44}{section.6.1}
\contentsline {chapter}{Literatura}{45}{chapter*.41}
\contentsline {chapter}{\numberline {A}Obsah DVD}{46}{appendix.A}
\contentsfinish 
